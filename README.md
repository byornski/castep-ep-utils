CASTEP Electron-Phonon Coupling Utils
=====================================

This is a collection of tools for manipulating electron-phonon coupling matrix elements from CASTEP output. These can be used to calculate transport properties among other things.


Examples of each script are given in the examples folder.


Superconductivity
=================

fermi_surface_qpts.py
---------------------
This uses the results of a density of states calculation to generate points of interest for further castep calculations.


ep_plot.py
----------
This calculates the superconducting transition temperature from outputs of fermi_surface_qpts.py.
