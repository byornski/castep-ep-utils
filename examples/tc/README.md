Example of tc.py
================

This folder contains sample data for electron-phonon supercondictivity transition temperature calculations.

Inputs
------
This script takes a .phonon and a .epme from the corresponding calculations from fermi_surface_qpts.py

```
python3 ../../ep_utils/tc.py -h
usage: tc.py [-h] [-ep EPME_FILE] [-ph PHONON_FILE] seedname nef mu

Reads castep .epme file and .phonon file to calculate superconducting
transition temperature

positional arguments:
  seedname              seedname to read files from -- both seedname.epme and
                        seedname-ph.phonon should be in the current folder
  nef                   DOS at the fermi energy (in eV^1)
  mu                    Screen coulomb interaction constant for the material

optional arguments:
  -h, --help            show this help message and exit
  -ep EPME_FILE, --epme_file EPME_FILE
                        Electron-phonon matrix elements file to read (.empe)
  -ph PHONON_FILE, --phonon_file PHONON_FILE
                        Phonon mode file to read (.phonon)
```

Example
-------
To generate the T_c, specifying a DOS at the Fermi energy of 0.4085, and a mu* of 0.11:

```
> python3 ../../ep_utils/tc.py Al 0.4085 0.11
Element: Al
EP Coupling Constant: 0.5944826608272221
Logarithmically averaged frequency: 230.97488619283683 K
T_c = 4.593533320849897K with mu*=0.11
```

There will also be several png files generated in the current folder with parts of the spectral function. The main spectral function will be in <seed>-a2f.png.

![Eliashberg Spectral Function for Al](Al-a2f.png)