fermi_surface_qpts.py example
=============================

This script generates some sampling points for an electron-phonon superconductivity calculation. It takes a <seed>.cell and a <seed>.bands file to generate a <seed>-ph.cell and a <seed>-ep.cell with the specified number of points in each.

Example
-------
The script only takes a seedname and optionally, a number of qpts for cell file:

```
> python3 ../../ep_utils/fermi_surface_qpts.py -h
usage: fermi_surface_qpts.py [-h] [-ph NUM_PH] [-ep NUM_EP] seedname

Finds the fermi surface from a DOS calculation and generates electron-phonon
input files

positional arguments:
  seedname              Seedname of .bands file. eg Al for Al.bands

optional arguments:
  -h, --help            show this help message and exit
  -ph NUM_PH, --num_ph NUM_PH
                        Number of phonon interpolation points to generate
  -ep NUM_EP, --num_ep NUM_EP
                        Number of electron-phonon pairs to generate
```

The defaults are equivalent to -ph 10000 -ep 100. An example using the inputs supplied is shown:

```
python3 ../../ep_utils/fermi_surface_qpts.py Al
Number of fermi surface points: 1465
Writing EP_KPOINT_PAIRS to Al-ep.cell: 100 pairs
Writing PHONON_FINE_KPOINT_LIST to Al-ph.cell: 10000 pairs
```

These cell files should be used as inputs for further CASTEP runs. This can then be analysed with tc.py to generate the superconducting transition temperature.