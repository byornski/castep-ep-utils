#!/usr/bin/env python3
"""
Script to generate electron-phonon mediated supercohducting transition
temperature from Castep electron-phonon data.
"""
from math import exp, log
import argparse
import matplotlib.pyplot as mp
import scipy.integrate as integrate
from scipy.interpolate import interp1d
import scipy.constants as sc
from readers.phonon_reader import PhononReader
from readers.epme_reader import EpmeReader


def dividew(xpoints, ypoints):
    '''Divide the y-points by the x-points'''
    return [y / x for x, y in zip(xpoints, ypoints)]


def logw(xpoints, ypoints):
    '''Calculate the logarithmically averaged y-points'''
    return [log(x) * y / x for x, y in zip(xpoints, ypoints)]


def integratew(xpoints, ypoints):
    '''Integrate reciprocally weighted points'''
    return integrate.simps(dividew(xpoints, ypoints), xpoints)


def integratewlog(xpoints, ypoints):
    '''Integrate logarithmically weighted points'''
    return integrate.simps(logw(xpoints, ypoints), xpoints)


def plot_func(filename, xpts, ypts, ylabel):
    '''Plot the function given to a file'''
    atomic_to_THz = 1/sc.value("atomic unit of time") / (2 * sc.pi * 10**12)
    mp.figure()
    mp.xlabel("Frequency (THz)")
    mp.ylabel(ylabel)
    mp.plot(xpts * atomic_to_THz, ypts, 'r-')
    mp.savefig(filename)


def generate_a2f(name, ep_filename, ph_filename, fermi_dos, bin_size=50):
    '''Reads EMPE and phonon data and generates the eliashberg spectral function a2f(w)'''

    # Read phonon and ep data from respective files
    epdata = EpmeReader(ep_filename)
    phdata = PhononReader(ph_filename)

    # Generate weighted phonon dos (ie F)
    xpts, ph_y = phdata.plot(width=0.01)

    # Normalise phonon data to n(Ef)
    ph_norm = integrate.simps(ph_y, xpts)
    ph_y *= (fermi_dos / epdata.cell_volume) / ph_norm
    plot_func(name+"-f.png", xpts, ph_y, r"$F(\omega)$")

    # Generate an average epme as a function of frequency
    epf = epdata.get_interpolator(bin_size=bin_size, energy_tol=0.01)

    # Generate a^2 on those same points
    ep_y = epf(xpts)
    plot_func(name+"-a2.png", xpts, ep_y, r"$\alpha^2(\omega)$")

    # Full a2f is the product of these contributions
    tot_points = ep_y * ph_y

    # Plot this
    plot_func(name + "-a2f.png", xpts, tot_points,
              r"Eliashberg Spectral Function, $\alpha^2F(\omega)$")

    # Make an interpolator for the spectral function
    spectral_function = interp1d(xpts, tot_points, fill_value=0, bounds_error=False)

    # Integrate to get the ep coupling constant
    ep_coupling_constant = integratew(xpts, tot_points)


    # Calculate logarithmically averaged frequency
    wlog = exp(integratewlog(xpts, tot_points) / ep_coupling_constant)

    # Convert to kelvin (from energy)
    Ha = sc.value('Hartree energy')
    Kb = sc.value('Boltzmann constant')
    wlog_si = wlog * Ha / Kb

    return spectral_function, ep_coupling_constant, wlog_si


def transition_temp(ep_constant, mu, wlog):
    '''Calculate superconducting transition temperature from McMillan formula'''
    t1 = 1.04 * (1.0 + ep_constant)
    t2 = ep_constant - mu * (1.0 + 0.62 * ep_constant)
    if t1 < 0 or t2 < 0:
        print("mu too big or lambda too small!")
    expterm = exp(-t1 / t2)
    t_factor = wlog / 1.20
    return t_factor * expterm


def calculate_tc(element,fermi_dos_per_ev, mu, ep_filename, ph_filename):
    '''Generate the spectral function and calculate Tc for a material'''
    print(f"Element: {element}")

    eV = sc.value('Hartree energy in eV')

    fermi_dos = fermi_dos_per_ev * eV

    a2f, ep_lambda, wlog = generate_a2f(element, ep_filename, ph_filename, fermi_dos)

    print(f"EP Coupling Constant: {ep_lambda}")
    print(f"Logarithmically averaged frequency: {wlog} K")
    tc = transition_temp(ep_lambda, mu, wlog)
    print(f"T_c = {tc}K with mu*={mu}")


def main():
    # Parse arguments and calculate
    parser = argparse.ArgumentParser(description="Reads castep .epme file and .phonon file"
                                     " to calculate superconducting transition temperature")
    parser.add_argument("seedname", help="seedname to read files from -- both seedname.epme"
                        " and seedname-ph.phonon should be in the current folder")


    parser.add_argument("nef", help="DOS at the fermi energy (in eV^1)", type=float)
    parser.add_argument("mu", default=0.11, type=float, help=
                        "Screen coulomb interaction constant for the material")

    parser.add_argument("-ep", "--epme_file", type=str, default=None,
                        help="Electron-phonon matrix elements file to read (.empe)")
    parser.add_argument("-ph", "--phonon_file", type=str, default=None,
                        help="Phonon mode file to read (.phonon)")


    args = parser.parse_args()

    if args.phonon_file is None:
        args.phonon_file = args.seedname + ".phonon"

    if args.epme_file is None:
        args.epme_file = args.seedname + ".epme"

    calculate_tc(args.seedname, args.nef, args.mu, args.epme_file, args.phonon_file)


if __name__ == "__main__":
    main()
